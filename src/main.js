import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as VueGoogleMaps from 'gmap-vue'
import config from './config.js';
import VueGeolocationApi from 'vue-geolocation-api'

Vue.config.productionTip = false
Vue.use(VueGeolocationApi/*, { ...options } */);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAaKkQ1hMUnqg9GBP_97QUJeQADyaSIqYU',
    libraries: 'places',
  },
  installComponents: true
})
Vue.prototype.$config = config
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
