import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/vehicules',
    name: 'Vehicules',
    component: () => import(/* webpackChunkName: "about" */ '../views/Vehicules.vue')
  },
  {
    path: '/vehicules/:id',
    name: 'vehicule',
    component: () => import(/* webpackChunkName: "about" */ '../views/singleVehicule.vue')
  },
  {
    path: '/magazines',
    name: 'magazines',
    component: () => import(/* webpackChunkName: "about" */ '../views/Magazines.vue')
  },
  {
    path: '/services',
    name: 'services',
    component: () => import(/* webpackChunkName: "about" */ '../views/Services.vue')
  },
  {
    path: '/services/:id',
    name: 'un service',
    component: () => import(/* webpackChunkName: "about" */ '../views/singleService.vue')
  },
  {
    path: '/inscription',
    name: 's\'inscrire a autoshop',
    component: () => import(/* webpackChunkName: "about" */ '../views/inscription.vue')
  },
  {
    path: '/connexion',
    name: 'se connecter sur autoshop',
    component: () => import(/* webpackChunkName: "about" */ '../views/connexion.vue')
  },
  {
    path: '/boutique',
    name: 'boutiques',
    component: () => import('../views/Boutiques.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/annonce/create',
    name: 'annonce',
    component: () => import('../views/AnnoncesCreate.vue')
  },

  {
    path: "/magazine/viewer/:id",
    name: 'MagazineViewer',

    component: ()=> import("../views/PdfViewer.vue")
  }
  
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
