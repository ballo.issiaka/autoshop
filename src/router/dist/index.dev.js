"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _Home = _interopRequireDefault(require("../views/Home.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

_vue["default"].use(_vueRouter["default"]);

var routes = [{
  path: '/',
  name: 'Home',
  component: _Home["default"]
}, {
  path: '/vehicules',
  name: 'Vehicules',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/Vehicules.vue'));
    });
  }
}, {
  path: '/vehicules/:id',
  name: 'vehicule',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/singleVehicule.vue'));
    });
  }
}, {
  path: '/magazines',
  name: 'magazines',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/Magazines.vue'));
    });
  }
}, {
  path: '/services',
  name: 'services',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/Services.vue'));
    });
  }
}, {
  path: '/services/:id',
  name: 'un service',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/singleService.vue'));
    });
  }
}, {
  path: '/inscription',
  name: 's\'inscrire a autoshop',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/inscription.vue'));
    });
  }
}, {
  path: '/connexion',
  name: 'se connecter sur autoshop',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/connexion.vue'));
    });
  }
}, {
  path: '/boutique',
  name: 'boutiques',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/Boutiques.vue'));
    });
  }
}, {
  path: '/about',
  name: 'About',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/About.vue'));
    });
  }
}, {
  path: '/annonce/create',
  name: 'annonce',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/AnnoncesCreate.vue'));
    });
  }
}];
var router = new _vueRouter["default"]({
  routes: routes,
  mode: 'history'
});
var _default = router;
exports["default"] = _default;