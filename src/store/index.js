import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


import autoshopModule from './modules/autoshop/index.js';

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    autoshop:autoshopModule
  }
})
