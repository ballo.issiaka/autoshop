"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _config = _interopRequireDefault(require("../../../config"));

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  loadMagazine: function loadMagazine(context) {
    var response, responseData, magazines;
    return regeneratorRuntime.async(function loadMagazine$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "magazine/all"));

          case 2:
            response = _context.sent;
            _context.next = 5;
            return regeneratorRuntime.awrap(response.data.allMagazine);

          case 5:
            responseData = _context.sent;
            magazines = [];
            responseData.forEach(function (data) {
              magazines.push({
                id: data._id,
                titre: data.titre,
                numero: data.numero,
                description: data.description,
                image: data.image,
                lien: data.lien
              });
            });
            context.commit('getMagazines', magazines);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    });
  },
  INSERT_ANNONCES: function INSERT_ANNONCES(context, payload) {
    var config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: "Bearer ".concat(payload.token)
      }
    };

    _axios["default"].post(config.DEFAULT_URL + '/vehicule/annonce', payload.data, config).then(function (response) {
      console.log(response.data);
    });
  },
  INSERT_SERVICES: function INSERT_SERVICES(context, payload) {
    var config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: "Bearer ".concat(payload.token)
      }
    };

    _axios["default"].post(config.DEFAULT_URL + '/service/annonce', payload.data, config).then(function (response) {
      console.log(response.data);
    });
  },
  INSERT_MATERIELS: function INSERT_MATERIELS(context, payload) {
    var config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: "Bearer ".concat(payload.token)
      }
    };

    _axios["default"].post(config.DEFAULT_URL + '/materiel/annonce', payload.data, config).then(function (response) {
      console.log(response.data);
    });
  },
  loadBanniere: function loadBanniere(context) {
    var response, responseData, bannieres;
    return regeneratorRuntime.async(function loadBanniere$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "banniere/all"));

          case 2:
            response = _context2.sent;
            _context2.next = 5;
            return regeneratorRuntime.awrap(response.data.allBanniere);

          case 5:
            responseData = _context2.sent;
            bannieres = [];
            responseData.forEach(function (data) {
              bannieres.push({
                lien: data.lien,
                image: data.image,
                page: data.page
              });
            });
            context.commit('getBannieres', bannieres);

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    });
  },
  loadService: function loadService(context) {
    var response, responseData, services;
    return regeneratorRuntime.async(function loadService$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "allservices"));

          case 2:
            response = _context3.sent;
            _context3.next = 5;
            return regeneratorRuntime.awrap(response.data.allService);

          case 5:
            responseData = _context3.sent;
            services = [];
            responseData.forEach(function (data) {
              services.push(data);
            });
            context.commit('getServices', services);

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    });
  },
  loadCategorie: function loadCategorie(context) {
    var response, responseData, categorie;
    return regeneratorRuntime.async(function loadCategorie$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "sous-categorie-service/all"));

          case 2:
            response = _context4.sent;
            _context4.next = 5;
            return regeneratorRuntime.awrap(response.data.allCategorie);

          case 5:
            responseData = _context4.sent;
            categorie = [];
            responseData.forEach(function (data) {
              categorie.push(data);
            });
            context.commit('getCategories', categorie);

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    });
  },
  loadCategorieMateriel: function loadCategorieMateriel(context) {
    var response, responseData, categorie;
    return regeneratorRuntime.async(function loadCategorieMateriel$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "sous-categorie-materiel/all"));

          case 2:
            response = _context5.sent;
            _context5.next = 5;
            return regeneratorRuntime.awrap(response.data.allCategorie);

          case 5:
            responseData = _context5.sent;
            categorie = [];
            responseData.forEach(function (data) {
              categorie.push(data);
            });
            console.log("categorie materiel", categorie);
            context.commit('getCategoriesMateriel', categorie);

          case 10:
          case "end":
            return _context5.stop();
        }
      }
    });
  },
  loadVehicules: function loadVehicules(context) {
    var response, responseData, vehicules;
    return regeneratorRuntime.async(function loadVehicules$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "vehicule/all"));

          case 2:
            response = _context6.sent;
            _context6.next = 5;
            return regeneratorRuntime.awrap(response.data.allVehicule);

          case 5:
            responseData = _context6.sent;
            vehicules = [];
            responseData.forEach(function (data) {
              vehicules.push(data);
            });
            context.commit('getVehicules', vehicules);

          case 9:
          case "end":
            return _context6.stop();
        }
      }
    });
  },
  loadTypeVehicules: function loadTypeVehicules(context) {
    var response, responseData, type;
    return regeneratorRuntime.async(function loadTypeVehicules$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "typevehicule/all"));

          case 2:
            response = _context7.sent;
            _context7.next = 5;
            return regeneratorRuntime.awrap(response.data.allTypeVehicule);

          case 5:
            responseData = _context7.sent;
            type = [];
            responseData.forEach(function (data) {
              type.push(data);
            });
            context.commit('getTypeVehicules', type);

          case 9:
          case "end":
            return _context7.stop();
        }
      }
    });
  },
  loadMarqueVehicules: function loadMarqueVehicules(context) {
    var response, responseData, marques;
    return regeneratorRuntime.async(function loadMarqueVehicules$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "marque/all"));

          case 2:
            response = _context8.sent;
            _context8.next = 5;
            return regeneratorRuntime.awrap(response.data.allMarque);

          case 5:
            responseData = _context8.sent;
            marques = [];
            responseData.forEach(function (data) {
              marques.push(data);
            });
            context.commit('getMarqueVehicules', marques);

          case 9:
          case "end":
            return _context8.stop();
        }
      }
    });
  },
  loadMateriels: function loadMateriels(context) {
    var response, responseData, materiels;
    return regeneratorRuntime.async(function loadMateriels$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "allmateriels"));

          case 2:
            response = _context9.sent;
            _context9.next = 5;
            return regeneratorRuntime.awrap(response.data.allMateriel);

          case 5:
            responseData = _context9.sent;
            materiels = [];
            responseData.forEach(function (data) {
              materiels.push(data);
            });
            context.commit('getMateriel', materiels);

          case 9:
          case "end":
            return _context9.stop();
        }
      }
    });
  },
  loadArticles: function loadArticles(context) {
    var response, responseData, articles;
    return regeneratorRuntime.async(function loadArticles$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return regeneratorRuntime.awrap((0, _axios["default"])(_config["default"].DEFAULT_URL + "article/all"));

          case 2:
            response = _context10.sent;
            _context10.next = 5;
            return regeneratorRuntime.awrap(response.data.allArticle);

          case 5:
            responseData = _context10.sent;
            articles = [];
            responseData.forEach(function (data) {
              articles.push(data);
            });
            context.commit('getArticles', articles);

          case 9:
          case "end":
            return _context10.stop();
        }
      }
    });
  }
};
exports["default"] = _default;