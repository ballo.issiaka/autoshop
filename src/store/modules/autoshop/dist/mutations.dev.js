"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  getBannieres: function getBannieres(state, payload) {
    //console.log(payload);
    state.bannieres = payload;
  },
  getArticles: function getArticles(state, payload) {
    state.articles = payload;
  },
  getMagazines: function getMagazines(state, payload) {
    state.magazines = payload;
  },
  getServices: function getServices(state, payload) {
    state.services = payload;
  },
  getCategories: function getCategories(state, payload) {
    state.categories = payload;
  },
  getCategoriesMateriel: function getCategoriesMateriel(state, payload) {
    state.categoriesMateriels = payload;
  },
  getMateriel: function getMateriel(state, payload) {
    state.materiels = payload;
  },
  getVehicules: function getVehicules(state, payload) {
    state.vehicules = payload;
  },
  getTypeVehicules: function getTypeVehicules(state, payload) {
    state.typeVehicule = payload;
  },
  getMarqueVehicules: function getMarqueVehicules(state, payload) {
    state.marqueVehicule = payload;
  }
};
exports["default"] = _default;