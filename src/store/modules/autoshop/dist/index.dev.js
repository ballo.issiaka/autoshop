"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mutations = _interopRequireDefault(require("./mutations.js"));

var _actions = _interopRequireDefault(require("./actions.js"));

var _getters = _interopRequireDefault(require("./getters.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  namespaced: true,
  state: function state() {
    return {
      bannieres: [],
      articles: [],
      categories: [],
      categorieArticles: [],
      boutiques: [],
      materiels: [],
      services: [],
      magazines: [],
      marques: [],
      vehicules: []
    };
  },
  mutations: _mutations["default"],
  actions: _actions["default"],
  getters: _getters["default"]
};
exports["default"] = _default;