"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  bannieres: function bannieres(state) {
    return state.bannieres;
  },
  articles: function articles(state) {
    return state.articles;
  },
  categorieArticles: function categorieArticles(state) {
    return state.categorieArticles;
  },
  boutiques: function boutiques(state) {
    return state.boutiques;
  },
  services: function services(state) {
    return state.services;
  },
  categories: function categories(state) {
    return state.categories;
  },
  materiels: function materiels(state) {
    return state.materiels;
  },
  magazines: function magazines(state) {
    return state.magazines;
  },
  marques: function marques(state) {
    return state.marques;
  },
  vehicules: function vehicules(state) {
    return state.vehicules;
  }
};
exports["default"] = _default;