

import config from "../../../config";
import axios from "axios";

export default {

  async loadMagazine(context) {
    const response = await axios(
      config.DEFAULT_URL+"magazine/all"
    );
    const responseData = await response.data.allMagazine;
    const magazines = [];
    responseData.forEach((data)=>{

       magazines.push({
        id:data._id, 
        titre: data.titre,
        numero: data.numero,
        description: data.description,
        image: data.image,
        lien: data.lien
       })

    });
    context.commit('getMagazines', magazines);
  },


  INSERT_ANNONCES(context, payload) {
    let config = {
        headers: {
            'content-type': 'multipart/form-data',
            Authorization: `Bearer ${payload.token}`
        }
    }
    axios.post(config.DEFAULT_URL + '/vehicule/annonce', payload.data, config).then((response) => {
        console.log(response.data)
    })
  },
  INSERT_SERVICES(context, payload) {
    let config = {
        headers: {
            'content-type': 'multipart/form-data',
            Authorization: `Bearer ${payload.token}`
        }
    }
    axios.post(config.DEFAULT_URL + '/service/annonce', payload.data, config).then((response) => {
        console.log(response.data)
    })
  },
  INSERT_MATERIELS(context, payload) {
      let config = {
          headers: {
              'content-type': 'multipart/form-data',
              Authorization: `Bearer ${payload.token}`
          }
      }
      axios.post(config.DEFAULT_URL + '/materiel/annonce', payload.data, config).then((response) => {
          console.log(response.data)
      })
  },


  async loadBanniere(context){
        const response = await axios(
      config.DEFAULT_URL+"banniere/all"
    );
        const responseData = await response.data.allBanniere;

        const bannieres = [];

        responseData.forEach((data)=>{

          bannieres.push({

            lien:data.lien,
            image:data.image,
            page:data.page

          })
        });

            context.commit('getBannieres', bannieres);

  },


  async loadService(context){

   const response = await axios(
      config.DEFAULT_URL+"allservices"
    )
    const responseData = await response.data.allService
    const services = []
    responseData.forEach((data)=>{
      services.push(data)
    })
    context.commit('getServices', services)
  },

  async loadCategorie(context){
    const response = await axios(
       config.DEFAULT_URL+"sous-categorie-service/all"
     )
     const responseData = await response.data.allCategorie
     const categorie = []
     responseData.forEach((data)=>{
       categorie.push(data)
     })
     context.commit('getCategories', categorie)
   },

     async loadCategorieMateriel(context){
    const response = await axios(
       config.DEFAULT_URL+"sous-categorie-materiel/all"
     )
     const responseData = await response.data.allCategorie
     const categorie = []
     responseData.forEach((data)=>{
       categorie.push(data)
     })
     console.log("categorie materiel",categorie);
     context.commit('getCategoriesMateriel', categorie)
   },

     async loadVehicules(context){
    const response = await axios(
       config.DEFAULT_URL+"vehicule/all"
     )
     const responseData = await response.data.allVehicule
     const vehicules = []
     responseData.forEach((data)=>{
       vehicules.push(data)
     })
     context.commit('getVehicules', vehicules)
   },


    async loadTypeVehicules(context){
    const response = await axios(
       config.DEFAULT_URL+"typevehicule/all"
     )
     const responseData = await response.data.allTypeVehicule
     const type = []
     responseData.forEach((data)=>{
       type.push(data)
     })
     context.commit('getTypeVehicules', type);
   },


    async loadMarqueVehicules(context){
    const response = await axios(
       config.DEFAULT_URL+"marque/all"
     )
     const responseData = await response.data.allMarque
     const marques = []
     responseData.forEach((data)=>{
       marques.push(data)
     })
     context.commit('getMarqueVehicules', marques);
   },

   
     async loadMateriels(context){
    const response = await axios(
       config.DEFAULT_URL+"allmateriels"
     )
     const responseData = await response.data.allMateriel
     const materiels = []
     responseData.forEach((data)=>{
       materiels.push(data)
     })
     context.commit('getMateriel', materiels)
   },


        async loadArticles(context){
    const response = await axios(
       config.DEFAULT_URL+"article/all"
     )
     const responseData = await response.data.allArticle
     const articles = []
     responseData.forEach((data)=>{
       articles.push(data)
     })
     context.commit('getArticles', articles)
   },

   



}