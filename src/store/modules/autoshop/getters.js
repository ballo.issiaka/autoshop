

export default {
  bannieres(state) {

    return state.bannieres;
  },
  articles(state) {
    return state.articles;
  },
  categorieArticles(state){
      return state.categorieArticles;
  },
   categorieMateriels(state){
      return state.categoriesMateriels;
  },
  boutiques(state){
      return state.boutiques;
  },
  services(state){
    return state.services;
  },
  categories(state){
    return state.categories;
  },
  materiels(state){
      return state.materiels;
  },
  magazines(state){
      return state.magazines;
  },
  marques(state){
      return state.marques;
  },

  vehicules(state){
      return state.vehicules;
  },

  typeVehicule(state){
     return state.typeVehicule;
  },

  marqueVehicule(state){
    return state.marqueVehicule;
  }
};