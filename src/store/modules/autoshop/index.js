
import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';


export default {
  namespaced: true,
  state() {
    return {
     bannieres : [],
     articles : [],
     categories: [],
     categoriesMateriels:[],
     categorieArticles : [],
     boutiques:[],
     materiels:[],
     services:[],
     magazines:[],
     marques:[],
     vehicules:[],
     typeVehicule:[],
     marqueVehicule:[]
     
    };
  },
  mutations,
  actions,
  getters
};
