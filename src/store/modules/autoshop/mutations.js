
export default {

  getBannieres(state,payload){

    //console.log(payload);
      state.bannieres = payload;
    
  },

  getArticles(state,payload){
        state.articles = payload;
  },

  getMagazines(state,payload){
     state.magazines = payload;
  },

  getServices(state,payload){
      state.services = payload;
  },
  getCategories(state,payload){
    state.categories = payload;
},

 getCategoriesMateriel(state,payload){
      state.categoriesMateriels = payload;
 },

 getMateriel(state,payload){
      state.materiels = payload;
 },
  getVehicules(state,payload){
    state.vehicules = payload;
},

  getTypeVehicules(state,payload){
    state.typeVehicule = payload;
},

  getMarqueVehicules(state,payload){
    state.marqueVehicule = payload;
},


};